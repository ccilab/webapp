*ちょう助を新しくする企画*
---
## ●システム構成図
[ちょう助のシステム構成図（created by 西山）](https://drive.google.com/open?id=1ZwqK89bo2lr1QvekqlfGC70bOXFYjFKI)

---
## ●サーバー起動
### ・オプションなし
```
python manage.py runserver
```
### ・ポート指定
```
python manage.py runserver 8080
```
### ・全ての IP からのリクエストを受け付ける
```
python manage.py runserver 0:8000
```
## ●確認
```
http://127.0.0.1:port/app_name
ex. http://127.0.0.1:8000/fullcalendar/
```
---
## ●開発フロー
### ・1タスクにつき 1Issue を切る
### ・開発ブランチについて
```
develop からブランチを切って開発する．
develop から merge する(最新化する)
develop に Pull reqest を投げる
```
### ・ブランチの命名ルール
```
description は端的にわかりやすく書いてください。
feature/${description}：新機能実装(テスト込み)
hotfix/${description}：最優先で直すバグ
fix/${description}：不具合修正(優先度低めなやつ)
${issue_num}は付けられたら付ける．
```
### ・コミットについて
### ・プルリクエストについて