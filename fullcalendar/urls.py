# 独自のURLconf
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='fullcalendar')
]
